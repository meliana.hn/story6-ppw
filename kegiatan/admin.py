from django.contrib import admin

from .models import Orang, Kegiatan

# Register your models here.
admin.site.register(Orang)
admin.site.register(Kegiatan)
