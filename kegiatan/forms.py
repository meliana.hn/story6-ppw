from django import forms


class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(
        label="Activity name ",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control add-form',
            }
        )
    )

    deskripsi_kegiatan = forms.CharField(
        label="Description ",
        max_length=1000,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control add-form',
            }
        )
    )


class FormOrang(forms.Form):
    nama_orang = forms.CharField(
        label="Name ",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control add-form',
            }
        )
    )
