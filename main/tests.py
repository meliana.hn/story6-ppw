from django.test import LiveServerTestCase, TestCase, tag
from selenium import webdriver
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .views import home
from .apps import MainConfig

class Test(TestCase):
    def test_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_func(self):
        comp = resolve('/')
        self.assertEqual(comp.func, home)

    def test_landing_page_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'main/home.html')


# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


# class MainTestCase(TestCase):
#     def test_root_url_status_200(self):
#         response = self.client.get('/')
#         self.assertEquals(response.status_code, 200)
#         # response = self.client.get(reverse('main:home'))
#         # self.assertEqual(response.status_code, 200)
  
#     def test_apakah_di_halaman_formulir_ada_templatenya(self):
#         response = self.client.get('/')
#         self.assertTemplateUsed(response, 'main/home.html')


# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
