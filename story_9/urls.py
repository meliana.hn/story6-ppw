from django.urls import path
from .views import loginUser, registerUser, logoutUser

urlpatterns = [
    path('', loginUser),
    path('register/', registerUser),
    path('logout/', logoutUser),
]
