from django.urls import path
from .views import bukuViews, jsonData

urlpatterns = [
    path('', bukuViews),
    path('jsondata/', jsonData),
]
