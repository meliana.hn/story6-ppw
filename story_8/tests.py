from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import bukuViews
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest(TestCase):

    def test_url_is_exist(self):
        response = self.client.get('/booksearch/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_fires_correct_views_method(self):
        reseponse = resolve('/booksearch/')
        self.assertEqual(reseponse.func, bukuViews)

    def test_json_data(self):
        response = self.client.get('/booksearch/jsondata/')
        self.assertEqual(response.status_code, 200)
        
    def test_views_render_correct_html(self):
        response = self.client.get('/booksearch/')
        self.assertTemplateUsed(response, 'story_8/buku.html')
